<?php

/**
 * @file
 * uw_cfg_devel.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_devel_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'coder_reviews';
  $strongarm->value = array(
    'style' => 'style',
    'comment' => 'comment',
    'sql' => 'sql',
    'security' => 'security',
    'i18n' => 'i18n',
    'upgrade47' => 0,
    'upgrade5x' => 0,
    'upgrade6x' => 0,
    'upgrade7x' => 0,
    'upgrade8x' => 0,
    'sniffer' => 0,
    'druplart' => 0,
    'production' => 0,
    'i18n_po' => 0,
  );
  $export['coder_reviews'] = $strongarm;

  return $export;
}
